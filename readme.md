Installation
============
```bash
git clone git@bitbucket.org:beesightsoft/okadabook.api.git 
cd okadabook.api
composer install
php artisan  key:generate 
```

Update environment
==================
```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=okadaboo_revise
DB_USERNAME=root
DB_PASSWORD=
```

Create a modular
================
[artisan-commands](https://nwidart.com/laravel-modules/v1/advanced-tools/artisan-commands)



